<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_test extends CI_Controller {

  function __construct(){
		parent::__construct();
		$this->load->model('m_test');
	}

  public function index()
  {
    $data['master_ct'] = $this->m_test->get_ct();
    $data['p_q_tarif'] = $this->m_test->get_quote_tarif_promo();
    $data['photo_q_tarif'] = $this->m_test->get_quote_tarif_photo();
    $data['video_q_tarif'] = $this->m_test->get_quote_tarif_video();
    $data['contents'] = 'contents/test/index';
    $this->load->view('master', $data);
  }

  public function add(){
    $post = $this->input->post(); //init

    //get input user
    foreach ($post['name'] as $key => $value) {
      $data_user[] = array (
          'name' => $post['name'][$key],
          'email' => $post['email'][$key],
          'cell_number' => $post['cell_number'][$key]
      );
    }
    $cek1 = $this->m_test->cek_usr($data_user[0]);
    $cek2 = $this->m_test->cek_usr($data_user[1]);
    if (($cek1==0) && ($cek2==0)){
      $this->m_test->input($data_user,'cp_users');
      
      $data_m_cont = $this->m_test->get_usr2();
      $terms = "[{'title':'".$post['title_master_contract_terms'][0]."','contract_terms':'".$post['contract_terms'][0]."'},[{'title':'".$post['title_master_contract_terms'][1]."','contract_terms':'".$post['contract_terms'][1]."'}";
      $last_id = $this->m_test->get_lastid();
      $id_table = $last_id[0]['id']+1;

      $data_cont = array (
        'contract_number' => 'C/'.date('y').'/'.$id_table,
        'event_date' => $post['event_date'],
        'id_users1' => $data_m_cont[1]['id'],
        'id_users2' => $data_m_cont[0]['id'],
        'nama1' => $data_m_cont[1]['name'],
        'nama2' => $data_m_cont[0]['name'],
        'contract_terms' => $terms
      );
      
      foreach ($post['id_tarif'] as $key => $value) {
        if ($post['id_tarif'][$key] != '') {
          $data_details[] = array (
              'id_contracts' => $id_table,
              'id_master_tarif' => $post['id_tarif'][$key],
              'nama_tarif' => $post['nama_tarif'][$key],
              'isi_paket' => $post['isi_paket'][$key],
              'harga' => $post['harga_tarif'][$key],
              'group' => $post['group'][$key]
          );
        }  
        $paket =  explode(",",$post['isi_paket'][$key]);
        foreach ($paket as $key1 => $value) {
          if ($paket[$key1] != '') {
            $data_package[] = array (
              'id_contract' => $id_table,
              'nama_tarif' => strtoupper($post['nama_tarif'][$key]),
              'isi_paket_to_do' => strtoupper($paket[$key1])
            );
          }
        }
      }
      $i = 1;
      foreach ($post['amount'] as $key => $value) {
        if ($post['amount'][$key] != '') {
          $data_payment[] = array (
              'id_contracts' => $id_table,
              'contract_number' => 'C/'.date('y').'/'.$id_table,
              'amount' => $post['amount'][$key],
              'due_date' => $post['due_date'][$key],
              'payment_sequence' => strtoupper($this->m_test->ordinal($i++)).' PAYMENT',
              'desc' => $post['desc'][$key]
          );
        }  
      }

      $kode = $this->m_test->get_kode();
      $kode_transaksi = intval($kode[0]['kode_transaksi'])+1;

      $data_jurnal[] = array (
        'id_contracts' => $id_table,
        'kode_transaksi' => $kode_transaksi,
        'no_coa' => '10201',
        'no_coa_head' => '102',
        'no_coa_utama' => '1',
        'nama_akun_coa' => 'PIUTANG CLIENT',
        'debit_kredit' => 'DEBIT',
        'jumlah_transaksi' => $post['grandtotal'],
        'tanggal_transaksi' => date('Y-m-d'),
        'ket' => 'PIUTANG CLIENT - Contract Number: C/'.date('y').'/'.$id_table,
        'created_date' => date('Y-m-d h:i:sa'),
        'created_by' => 'Client estimate'
      );
      $data_jurnal[] = array (
        'id_contracts' => $id_table,
        'kode_transaksi' => $kode_transaksi,
        'no_coa' => '40101',
        'no_coa_head' => '401',
        'no_coa_utama' => '4',
        'nama_akun_coa' => 'INCOME',
        'debit_kredit' => 'KREDIT',
        'jumlah_transaksi' => $post['subtotal'],
        'tanggal_transaksi' => date('Y-m-d'),
        'ket' => 'INCOME - Contract Number: C/'.date('y').'/'.$id_table,
        'created_date' => date('Y-m-d h:i:sa'),
        'created_by' => 'Client estimate'
      );
      $data_jurnal[] = array (
        'id_contracts' => $id_table,
        'kode_transaksi' => $kode_transaksi,
        'no_coa' => '20201',
        'no_coa_head' => '202',
        'no_coa_utama' => '2',
        'nama_akun_coa' => 'TAX',
        'debit_kredit' => 'KREDIT',
        'jumlah_transaksi' => $post['tax'],
        'tanggal_transaksi' => date('Y-m-d'),
        'ket' => 'TAX - Contract Number: C/'.date('y').'/'.$id_table,
        'created_date' => date('Y-m-d h:i:sa'),
        'created_by' => 'Client estimate'
      );

      $this->m_test->input_contract($data_cont,'cp_contracts');
      $this->m_test->input($data_details,'cp_contracts_details');
      $this->m_test->input($data_package,'cp_contracts_package');
      $this->m_test->input($data_payment,'cp_contracts_payment_plans');
      $this->m_test->input($data_jurnal,'cp_jurnal');
      echo json_encode(array(
				"statusCode"=>200,
				"message" => "Success"
			));
    } else {
      echo json_encode(array(
				"statusCode"=>401,
				"message" => "Failed (email/no hp telah terdaftar)"
			));
    }
    
  }
  

  public function cek(){
    $kode = $this->m_test->get_kode();
    echo $kode_transaksi = intval($kode[0]['kode_transaksi'])+1;
  }
}

/* End of file C_test.php */

?>
