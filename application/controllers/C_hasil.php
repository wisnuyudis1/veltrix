<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_hasil extends CI_Controller {

  function __construct(){
		parent::__construct();
		$this->load->model('m_test');
	}

  public function index()
  {
    $data['contents'] = 'contents/hasil/index';
    $this->load->view('master', $data);
  }
}
?>
