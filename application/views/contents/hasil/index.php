<div class="container-fluid">
    <head>

    </head>

    <div class="row align-items-center">
        <div class="col-sm-12">
            <div class="float-right d-none d-md-block">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h4 class="card-header mt-0">Quotation</h4>
                <div class="card-body">
                  <table class="table" id="table-quotation">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Client Names</th>
                            <th>Contract Number</th>
                            <th>Name-Email-Cell Number-Client 1</th>
                            <th>Name-Email-Cell Number-Client 2</th>
                            <th>Subtotal Fee</th>
                            <th>Tax</th>
                            <th>Grand Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var table = $('#table-quotation').DataTable();
    });
</script>