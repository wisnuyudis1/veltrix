<?php

class M_web extends CI_Model {

    var $table = 'cp_contracts'; //nama tabel dari database
    var $column_order = array(null, 'no_antrian','norm','nama','tgl_lahir','alamat','no_hp','tgl_kunjung','poli','nama_dokter','penjamin','jenis','tgl_input','status'); //field yang ada di table user
    var $column_search = array('no_antrian','norm','nama'); //field yang diizin untuk pencarian
    var $order = array('no_antrian' => 'asc'); // default order

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->like('tgl_input',date('Y-m-d'));
        $this->db->order_by('no_antrian','asc');
        $this->db->from($this->table);
        $this->db->join('cp_users','cp_users.id = cp_contract.id_users1');
        $i = 0;
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                    $this->db->like('tgl_input',date('Y-m-d'));
              	    $this->db->order_by('no_antrian','asc');
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                    $this->db->like('tgl_input',date('Y-m-d'));
              	    $this->db->order_by('no_antrian','asc');
                }

                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->like('tgl_input',date('Y-m-d'));
  	    $this->db->order_by('no_antrian','asc');
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
