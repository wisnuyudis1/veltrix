<?php
class M_test extends CI_Model{

  function get_quote_tarif_promo(){
    $this->db->where('group','PROMO PACKAGE');
    return $this->db->get('cp_master_quote_tarif');
  }

  function get_quote_tarif_photo(){
    $this->db->where('group','PHOTOGRAPHY PACKAGES');
    return $this->db->get('cp_master_quote_tarif');
  }

  function get_quote_tarif_video(){
    $this->db->where('group','VIDEOGRAPHY INVESTMENT');
    return $this->db->get('cp_master_quote_tarif');
  }

  function get_ct(){
    $this->db->limit(2);
    return $this->db->get('cp_master_contract_terms');
  }

  function ordinal($num) {
    if (!in_array(($num % 100),array(11,12,13))){
      switch ($num % 10) {
        // Handle 1st, 2nd, 3rd
        case 1:  return $num.'st';
        case 2:  return $num.'nd';
        case 3:  return $num.'rd';
      }
    }
    return $num.'th';
  }

  function get_kode(){
    $this->db->limit(1);
    $this->db->order_by('kode_transaksi','desc');
    return $this->db->get('cp_jurnal')->result_array();
  }

  function cek_usr($data){
    $this->db->where('email',$data['email']);
    $this->db->or_where('cell_number',$data['cell_number']);
    return $this->db->get('cp_users')->num_rows();
  }

  function input($data,$table){
    $this->db->insert_batch($table,$data);
  }

  function input_contract($data_user,$table){
    $this->db->insert($table,$data_user);
  }

  function get_lastid(){
    $this->db->limit(2);
    $this->db->order_by('id','desc');
    return $this->db->get('cp_contracts')->result_array();
  }

  function get_usr2(){
    $this->db->limit(2);
    $this->db->order_by('id','desc');
    return $this->db->get('cp_users')->result_array();
  }
}
